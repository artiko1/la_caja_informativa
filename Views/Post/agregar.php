<!DOCTYPE html>
<html>
<head>
	<title></title>
	<script src="Public/js/jquery-3.4.1.min.js"></script>
	<script src="Public/js/jquery.validate.min.js"></script>
	<script src="Public/js/validaciones.js"></script>

	<link rel="stylesheet" type="text/css" href="Public/css/style.css">
	<script src="https://cdn.tiny.cloud/1/iq0qbx3c53qqh3jebbsgzmw9ii6pjxlm5nzd1yz19sixd0bm/tinymce/5/tinymce.min.js"></script>
	<script src="Public/js/tinymce.js"></script>

</head>
<body>
	<?php include_once 'Views/Navegacion/Menu.php'; ?>
	<h2>Agregar Post</h2>

	<?php if (isset($errores) && count($errores) > 0): ?>
	<h5>Se han encontrado errores en el formulario</h5>

	<ul>
		<?php foreach ($errores as $key => $err) :?>

			<li><?php echo $err; ?></li>

		<?php endforeach?>
	</ul>

    <?php endif ?>

	<form method="post" action="index.php?controlador=post&accion=guardar" id="postForm" enctype="multipart/form-data">
		<table>
			<tr>
				<td>
					<label>USUARIO</label>
				</td>
				<td>
					<input type="number" name="txtIdUsuario" id="txtIdUsuario">
				</td>
			</tr>
			<tr>
				<td>
					<label>CATEGORIA</label>
				</td>
				<td>
					<select name="cboCategoria" id="cboCategoria">
						<option value="">Seleccionar</option>
						<<?php foreach ($categorias as $cat): ?>
							<option value="<?php echo $cat->getId_categoria(); ?>"><?php echo $cat->getDescripcion(); ?></option>
						<?php endforeach ?>
					</select>
				</td>
			</tr>
			<tr>
				<td>
					<label>FECHA</label>
				</td>
				<td>
					<input type="date" name="dpkFechaPost" id="dpkFechaPost">
				</td>
			</tr>
			<tr>
				<td>
					<label>TITULO</label>
				</td>
				<td>
					<input type="text" name="txtTitulo" id="txtTitulo">
				</td>
			</tr>
			<tr>
				<td>
					<label>DESCRIPCION POST</label>
				</td>
				<td>
					<input type="text" name="txtDescripcionPost" id="txtDescripcionPost">
				</td>
			</tr>
			<tr>
				<td>
					<label>CONTENIDO POST</label>
				</td>
				<td>
					<input type="text" name="txtContenidoPost" id="txtContenidoPost">
				</td>
			</tr>
			<tr>
				<td>
					<label for="txtImagen">Subir Imagen</label>
	   	            <input type="file" name="txtImagen" id="txtImagen">
				</td>
			</tr>
			<tr>
				<td>
					<input type="submit" name="btnGuardar" id="btnGuardar" value="Guardar">
				</td>
			</tr>
			
		</table>
	</form>
    <?php echo (isset($mensaje))?$mensaje: ''; ?>


</body>
</html>