<!DOCTYPE html>
<html>
<head>
	<title></title>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
</head>
<body>
	<?php include_once 'Views/Navegacion/Menu.php'; ?>
	<h2>Agregar Post</h2>
	<form method="post" action="index.php?controlador=post&accion=modificar" enctype="multipart/form-data">
		<table>
			<tr>
				<td>
					<label>USUARIO</label>
				</td>
				<td>
					<input type="text" name="txtIdUsuario" id="txtIdUsuario" value="<?php echo $post->getId_usuario()->getId_usuario();?>">
					<input type="hidden" name="txtIdPost" id="txtIdPost" value="<?php echo $post->getId_post(); ?>">
				</td>
			</tr>
			<tr>
				<td>
					<label>CATEGORIA</label>
				</td>
				<td>
					<select name="cboCategoria" id="cboCategoria">
						<option value="">Seleccionar</option>
						<?php foreach ($categorias as $cat): ?>
							<option value="<?php echo $cat->getId_categoria(); ?>"<?php echo ($cat->getId_categoria() == $post->getCategoria()->getId_categoria())? 'selected': ''?>><?php echo $cat->getDescripcion(); ?></option>
						<?php endforeach ?>
					</select>
				</td>
			</tr>
			<tr>
				<td>
					<label>FECHA</label>
				</td>
				<td>
					<input type="date" name="dpkFechaPost" id="dpkFechaPost" value="<?php echo $post->getFecha_post();?>">
				</td>
			</tr>
			<tr>
				<td>
					<label>TITULO</label>
				</td>
				<td>
					<input type="text" name="txtTitulo" id="txtTitulo" value="<?php echo $post->getTitulo_post();?>">
				</td>
			</tr>
			<tr>
				<td>
					<label>DESCRIPCION POST</label>
				</td>
				<td>
					<input type="text" name="txtDescripcionPost" id="txtDescripcionPost" value="<?php echo $post->getDescripcion_post();?>">
				</td>
			</tr>
			<tr>
				<td>
					<label>CONTENIDO POST</label>
				</td>
				<td>
					<input type="text" name="txtContenidoPost" id="txtContenidoPost" value="<?php echo $post->getContenido_post();?>">
				</td>
			</tr>
			<tr>
				<td>
					<label for="txtImagen">Subir Imagen</label>
					<img for="txtImagen" src="imagenes\<?php echo $post->getImagen_post();?>" height="100" width="100">
	   	            <input type="file" name="txtImagen" id="txtImagen" value="">

				</td>
			</tr>
			<tr>
				<td>
					<input type="submit" name="btnModificar" id="btnModificar" value="Modificar">
				</td>
			</tr>
		</table>
	</form>
    <?php echo (isset($mensaje))?$mensaje: ''; ?>
</body>
</html>