<!DOCTYPE html>
<html>
<head>
	<title>listado de post</title>
</head>
<body>
	<?php include_once 'Views/Navegacion/Menu.php'; ?>
	<h1>listado de post</h1>

	<table>
		<tr>
			<th>ID</th>
		    <th>Usuario</th>
		    <th>Categoria</th>
		    <th>Fecha post</th>
		    <th>Titulo post</th>
		    <th>Imagen post</th>
		    <th>Descripcion</th>
		    <th>Contenido</th>
		    <th>Herramientas</th>
		</tr>
	
	

	<?php foreach ($posts as $post): ?>
		<tr>
			<td><?php echo $post->getId_post();?></td>
			<td><?php echo $post->getId_usuario()->getNombre_usuario();?></td>
			<td><?php echo $post->getCategoria()->getDescripcion();?></td>
			<td><?php echo $post->getFecha_post();?></td>
			<td><?php echo $post->getTitulo_post();?></td>
			<td><?php echo $post->getImagen_post();?></td>
			<td><?php echo $post->getDescripcion_post();?></td>
			<td><?php echo $post->getContenido_post();?></td>
			<td>
				<a href="index.php?controlador=post&accion=eliminar&id=<?php echo $post->getId_post();?>">Eliminar</a>
				<a href="index.php?controlador=post&accion=modificar&id=<?php echo $post->getId_post();?>">Modificar</a>
			</td>
		</tr>
	<?php endforeach; ?>
    </table>
    <?php echo (isset($mensaje))?$mensaje: ''; ?>
</body>
</html>