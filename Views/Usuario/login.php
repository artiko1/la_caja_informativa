<!DOCTYPE html>
<html>
<head>
	<title>Login de usuario | La Caja Informativa</title>
</head>
<body>
	<h1>Ingrese sus credenciales</h1>
	<form method="post" action="index.php?controlador=login&accion=login">
		<table>
			<thead>
				<tr>
					<td>
						<label>Nombre Usuario</label>
					</td>
					<td>
						<input type="text" name="txtNombreUsuario">
					</td>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>
						<label>Contraseña</label>
					</td>
					<td>
						<input type="password" name="txtPassword" id="txtPassword">
					</td>
				</tr>
				<tr>
					<td></td>
					<td>
						<input type="submit" name="btnLogin" id="btnLogin" value="login">
					</td>
				</tr>
			</tbody>
		</table>
	</form>
	<?php echo (isset($mensaje))?$mensaje: ''; ?>
</body>
</html>