<?php

include_once 'Controllers/PostController.php';
include_once 'Controllers/LoginController.php';

$controlador = $_GET['controlador'];
$accion = $_GET['accion'];

session_start();

if ($controlador == "login" && $accion == "login") {
  $ctl = new LoginController();

  $ctl->login();
  return;
}

if (!isset($_SESSION['usuario'])) {
  header("location: index.php?controlador=login&accion=login");
}


//estos controladores y rutas estan protegidos con autenticacion
if ($controlador == "post") {
  $ctl = new PostController();

  if ($accion == "listar") {
    $ctl->listar();
  }elseif ($accion == "guardar") {
  	$ctl->guardar();
  }elseif ($accion == "eliminar") {
  	$ctl->eliminar();
  }elseif ($accion == "modificar") {
  	$ctl->modificar();
  }
}elseif ($controlador == "login") {
  $ctl = new LoginController();
  
  if ($accion == "cerrar") {
    $ctl->cerrarSesion();
  }
}
?>