
      tinymce.init({
        selector: '#txtContenidoPost',
        height: 500,
        width: 600,
        statusbar: false,
        menubar: false,
        language: "es",
        plugins: [
           "advlist autolink autosave link image lists charmap preview hr anchor spellchecker",
           "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonebreaking",
           "table contextmenu directionality emoticons template textcolor paste textcolor colorpicker textpattern"
        ],
        toolbar1: "newdocument | bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | cut copy paste | bullist numlist | outdent indent | undo redo | link unlink image media | preview | forecolor backcolor | formatselect",
        toolbar3: "subscript superscript | emoticons",

        toobar_items_size: 'small',
        content_css: [
         '//fast.fonts.net/cssapi/e6dc9b99-64fe-4292-ad98-6974f93cd2a2.css',
         '//www.tinymce.com/css/codepen.min.css'
        ]
      });