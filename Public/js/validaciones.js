
$(document).ready(function () {
	$("#postForm").validate({
		rules: {
			txtTitulo: {
				required: true,
				minlength:5,
				maxlength:20
			},

			cboCategorias: {
				required: true
			},

			txtIdUsuario: {
				required: true,
				number: true
			}
		},

		messages: {
			txtTitulo: {
				required: "Campo requerido",
				minlength: "Minimo de 5 caracteres",
				maxlength: "Maximo de 20 caracteres"
			}
		}
	});	
});
