<?php 
/**
 * 
 */
include_once 'Models/DAO/UsuarioDAO.php';
include_once 'Models/Entities/Usuario.php';
class LoginController
{
	private $usuarioDAO;

	function __construct()
	{
		$this->usuarioDAO = new UsuarioDAO();
	}
	
	public function login() {
		if (empty($_POST)) {
			include_once 'Views/Usuario/login.php';
			return;
		}

		$nombreUsuario = $_POST['txtNombreUsuario'];
		$password = $_POST['txtPassword'];

		$usuario = new Usuario();

		$usuario->setNombre_usuario($nombreUsuario);
		$usuario->setPassword($password);

		$usuarioValidado = $this->usuarioDAO->login($usuario);

		if ($usuarioValidado == null) {
			$mensaje = "credenciales incorrectas";

			include_once 'Views/Usuario/login.php';
			return;
		}

		$_SESSION['usuario'] = $usuarioValidado->getNombre_usuario();
		//añadir el nombre de usuario en la sesion
		header("location: index.php?controlador=post&accion=listar");
		//redirigir a un usuario a una pagina por defecto
	}


	public function cerrarSesion() {

		$_SESSION['usuario'] = null;

		header("location: index.php?controlador=login&accion=login");
	}
}
?>