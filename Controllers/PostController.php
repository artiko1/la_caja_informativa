<?php 
/**
 * 
 */
include_once 'Models/DAO/PostDAO.php';
include_once 'Models/DAO/CategoriaDAO.php';
include_once 'Models/Entities/Post.php';
include_once 'Models/Entities/Categoria.php';
include_once 'Models/Entities/Usuario.php';
class PostController
{
	private $postDAO;

	function __construct()
	{
		$this->postDAO = new PostDAO();
	}


	public function listar() {

		$posts = $this->postDAO->listarPost();

		include_once 'Views/Post/listar.php';
	}


	public function guardar() {

		$cDAO = new CategoriaDAO();
		$categorias = $cDAO->listarCategorias();

		if (empty($_POST)) {//la peticion fue GET
			include_once 'Views/Post/agregar.php';
			return;
		}

		$errores = $this->validador();

		if (count($errores) > 0) {
			include_once 'Views/Post/agregar.php';
			return;
		}

		$post = new Post();
		$categoria = new Categoria();

		$post->setId_usuario($_POST['txtIdUsuario']);
		$categoria->setId_categoria($_POST['cboCategoria']);
		$post->setCategoria($categoria);
		$post->setFecha_post($_POST['dpkFechaPost']);
		$post->setTitulo_post($_POST['txtTitulo']);
        
        //procesar una imagen
		$nombre_imagen = $_FILES["txtImagen"]["name"];
        $nombre_temporal = $_FILES["txtImagen"]["tmp_name"];
        $tipo_archivo = $_FILES["txtImagen"]["type"];
		$post->setImagen_post($nombre_imagen);

		$destino = "imagenes/" . $nombre_imagen;
		if ($tipo_archivo == "image/jpeg" || $tipo_archivo == "imagen/jpg" || $tipo_archivo == "image/png" || $tipo_archivo´== "image/gif") {
	    move_uploaded_file($nombre_temporal, $destino);
	    echo "la imagen se ha subido con exito";
        }else{
	    echo "El archivo no es una imagen. <br>";
	    return;
        }

		$post->setDescripcion_post($_POST['txtDescripcionPost']);
		$post->setContenido_post($_POST['txtContenidoPost']);

		$mensaje = "Error al guardar";

		if ($this->postDAO->agregarPost($post)) {
			$mensaje = "Guardado correctamente";
		}

		//$posts = $this->postDAO->listarPost();

		//include_once 'Views/Post/listar.php';
		include_once 'Views/Post/agregar.php';
	}


	public function eliminar() {

		$id = $_GET['id'];

		$mensaje = "Error al eliminar";

		if ($this->postDAO->eliminarPost($id)) {
			$mensaje= "Eliminado correctamente";
		}

		$posts = $this->postDAO->listarPost();

		include_once 'Views/Post/listar.php';

		
	}

	public function modificar() {

		$categoriaDAO = new CategoriaDAO();
		$categorias = $categoriaDAO->listarCategorias();

		if (empty($_POST)) {//peticion get
			if (!isset($_GET['id'])) {
				header("location: index.php?controlador=post&accion=listar");
			}

			$id = $_GET['id'];
			$post = $this->postDAO->buscarPost($id);
			include_once 'Views/Post/modificar.php';
			return;
		}

		$post = new Post();
		$categoria = new Categoria();

		
		$post->setId_usuario($_POST['txtIdUsuario']);
		$categoria->setId_categoria($_POST['cboCategoria']);
		$post->setCategoria($categoria);
		$post->setFecha_post($_POST['dpkFechaPost']);
		$post->setTitulo_post($_POST['txtTitulo']);

		//procesar una imagen
		$nombre_imagen = $_FILES["txtImagen"]["name"];
        $nombre_temporal = $_FILES["txtImagen"]["tmp_name"];
        $tipo_archivo = $_FILES["txtImagen"]["type"];
		$post->setImagen_post($nombre_imagen);

		$destino = "imagenes/" . $nombre_imagen;
		if ($tipo_archivo == "image/jpeg" || $tipo_archivo == "imagen/jpg" || $tipo_archivo == "image/png" || $tipo_archivo´== "image/gif") {
	    move_uploaded_file($nombre_temporal, $destino);
	    echo "la imagen se ha subido con exito";
        }else{
	    echo "El archivo no es una imagen. <br>";
	    return;
        }


		$post->setDescripcion_post($_POST['txtDescripcionPost']);
		$post->setContenido_post($_POST['txtContenidoPost']);
		$post->setId_post($_POST['txtIdPost']);

		$mensaje = "Error al modificar";

		if ($this->postDAO->modificarPost($post)) {
			$mensaje = "modificado correctamente";
		}

		$posts = $this->postDAO->listarPost();

		include_once 'Views/Post/listar.php';
	}



	public function validador() {
		$errores = [];
		if (!isset($_POST['txtTitulo']) || $_POST['txtTitulo'] == "") {
			$errores[] = "Campo titulo requerido"; 
		}elseif (strlen($_POST['txtTitulo']) < 5) {
			$errores[] = "Minimo de 5 caráteres";
		}

		if (!isset($_POST['cboCategoria']) || $_POST['cboCategoria'] == "") {
			$errores[] = "Seleccione un tipo";
		}

		if (!isset($_POST['txtIdUsuario']) || $_POST['txtIdUsuario'] == "") {
			$errores[] = "Campo id usuario requerido";
		}elseif (!is_numeric($_POST['txtIdUsuario'])) {
			$errores[] = "El campo id usuario debe ser numerico";
		}

		return $errores;
	}
}
?>