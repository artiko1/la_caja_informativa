<?php
/**
 * 
 */
class Post
{
    private $id_post;
    private $id_usuario;
    private $categoria;
    private $fecha_post;
    private $titulo_post;
    private $imagen_post;
    private $descripcion_post;
    private $contenido_post;

    function __construct() 
    {

    }

    function getId_post() {
    	return $this->id_post;
    }

    function setId_post($id_post) {
    	$this->id_post = $id_post;
    }

    function getId_usuario() {
    	return $this->id_usuario;
    }

    function setId_usuario($id_usuario) {
    	$this->id_usuario = $id_usuario;
    }

    function getCategoria() {
    	return $this->categoria;
    }

    function setCategoria($categoria) {
    	$this->categoria = $categoria;
    }

    function getFecha_post() {
    	return $this->fecha_post;
    }

    function setFecha_post($fecha_post) {
    	$this->fecha_post = $fecha_post;
    }

    function getTitulo_post() {
    	return $this->titulo_post;
    }

    function setTitulo_post($titulo_post) {
    	$this->titulo_post = $titulo_post;
    }

    function getImagen_post() {
    	return $this->imagen_post;
    }

    function setImagen_post($imagen_post) {
    	$this->imagen_post = $imagen_post;
    }

    function getDescripcion_post() {
    	return $this->descripcion_post;
    }

    function setDescripcion_post($descripcion_post) {
    	$this->descripcion_post = $descripcion_post;
    }

    function getContenido_post() {
    	return $this->contenido_post;
    }

    function setContenido_post($contenido_post) {
    	$this->contenido_post = $contenido_post;
    }
}
?>