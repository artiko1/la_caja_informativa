<?php 
/**
 * 
 */
class Usuario 
{
	private $id_usuario;
	private $id_tipo_usuario;
	private $nombre_usuario;
	private $email;
	private $password;
	private $nombre;
	private $apellido_paterno;
	private $apellido_materno;
	private $celular;
	
	function __construct()
	{
		
	}

	function getId_usuario() {
		return $this->id_usuario;
	}

	function setId_usuario($id_usuario){
		$this->id_usuario = $id_usuario;
	}

	function getId_tipo_usuario() {
		return $this->id_tipo_usuario;
	}

	function setId_tipo_usuario($id_tipo_usuario) {
		$this->id_tipo_usuario = $id_tipo_usuario;
	}

	function getNombre_usuario() {
		return $this->nombre_usuario;
	}

	function setNombre_usuario($nombre_usuario) {
		$this->nombre_usuario = $nombre_usuario;
	}

	function getEmail() {
		return $this->email;
	}

	function setEmail($email) {
		$this->email = $email;
	}

	function getPassword() {
		return $this->password;
	}

	function setPassword($password) {
		$this->password = $password;
	}

	function getNombre() {
		return $this->nombre;
	}

	function setNombre($nombre) {
		$this->nombre = $nombre;
	}

	function getApellido_paterno() {
		return $this->apellido_paterno;
	}

	function setApellido_paterno($apellido_paterno) {
		$this->apellido_paterno = $apellido_paterno;
	}

	function getApellido_materno() {
		return $this->apellido_materno;
	}

	function setApellido_materno($apellido_materno) {
		$this->apellido_materno = $apellido_materno;
	}

	function getCelular() {
		return $this->celular;
	}

	function setCelular($celular) {
		$this->celular = $celular;
	}
}
?>