<?php 
/**
 * 
 */
class Categoria
{
	private $id_categoria;
	private $descripcion;

	function __construct()
	{
	
	}

	function getId_categoria() {
		return $this->id_categoria;
	}

	function setId_categoria($id_categoria){
		$this->id_categoria = $id_categoria;
	}

	function getDescripcion() {
		return $this->descripcion;
	}

	function setDescripcion($descripcion) {
		$this->descripcion = $descripcion;
	}
}
?>