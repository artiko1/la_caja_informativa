<?php
/**
 * 
 */
class Review
{
	private $id_review;
    private $id_usuario;
    private $id_categoria;
    private $fecha_review;
    private $titulo_review;
    private $imagen_review;
    private $descripcion_review;
    private $contenido_review;
	function __construct()
	{
		
	}

	function getId_review() {
    	return $this->id_review;
    }

    function setId_post($id_review) {
    	$this->id_review = $id_review;
    }

    function getId_usuario() {
    	return $this->id_usuario;
    }

    function setId_usuario($id_usuario) {
    	$this->id_usuario = $id_usuario;
    }

    function getId_categoria() {
    	return $this->id_categoria;
    }

    function setId_categoria($id_categoria) {
    	$this->id_categoria = $id_categoria;
    }

    function getFecha_review() {
    	return $this->fecha_review;
    }

    function setFecha_post($fecha_review) {
    	$this->fecha_post = $fecha_review;
    }

    function getTitulo_review() {
    	return $this->titulo_review;
    }

    function setTitulo_review($titulo_review) {
    	$this->titulo_review = $titulo_review;
    }

    function getImagen_review() {
    	return $this->imagen_review;
    }

    function setImagen_review($imagen_review) {
    	$this->imagen_review = $imagen_review;
    }

    function getDescripcion_review() {
    	return $this->descripcion_review;
    }

    function setDescripcion_review($descripcion_review) {
    	$this->descripcion_review = $descripcion_review;
    }

    function getContenido_review() {
    	return $this->contenido_review;
    }

    function setContenido_post($contenido_post) {
    	$this->contenido_post = $contenido_post;
    }
}
?>