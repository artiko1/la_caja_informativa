<?php 

/**
* Clase que se conecta con la base de datos
*/
class Conexion
{
	private $host = "localhost";
	private $nombreUsuario = "root";
	private $password = "";
	private $bd = "caja_informativa";

	private $mysqli;

	function __construct()
	{
		
	}

	public function conectar() {

		$this->mysqli = new mysqli($this->host, $this->nombreUsuario, $this->password, $this->bd);

		return $this->mysqli;
	}
	
}
?>