<?php 
/**
 * 
 */
include_once 'Models/Entities/Usuario.php';
class UsuarioDAO
{
	private $mysqli;

	function __construct() {
		$conexion = new Conexion();
		$this->mysqli = $conexion->conectar();
	}


	public function login(Usuario $usuario) {

		$nombreUsuario = $usuario->getNombre_usuario();
		$password = $usuario->getPassword();

		$sql = "select * from usuarios where nombre_usuario = '$nombreUsuario' and password = '$password'";

		$resultado = $this->mysqli->query($sql);

		$row = $resultado->fetch_assoc();

		if ($row == null) {
			return null;
		}

		$usuarioValidado = new Usuario();

		$usuarioValidado->setNombre_usuario($row['nombre_usuario']);
		$usuarioValidado->setEmail($row['email']);
		$usuarioValidado->setNombre($row['nombre']);
		$usuarioValidado->setApellido_paterno($row['apellido_paterno']);
		$usuarioValidado->setApellido_materno($row['apellido_materno']);
		$usuarioValidado->setCelular($row['celular']);

		return $usuarioValidado;
	}

}
?>