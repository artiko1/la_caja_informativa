<?php 
/**
 * 
 */
include_once 'Models/Connection/Conexion.php';
include_once 'Models/Entities/Categoria.php';
class CategoriaDAO
{
	private $mysqli;

	function __construct()
	{
		$conexion = new Conexion();
		$this->mysqli = $conexion->conectar();
	}

	public function listarCategorias() {
		$sql = "select * from categorias";

		$respuesta = $this->mysqli->query($sql);

		$categorias = [];

		while ($row = $respuesta->fetch_assoc()) {
			$categoria = new Categoria();
			$categoria->setId_categoria($row['id_categoria']);
			$categoria->setDescripcion($row['descripcion']);

			$categorias[] = $categoria;
		}

		$this->mysqli->close();
		return $categorias;
	}
}
?>