<?php

include_once 'Models/Connection/Conexion.php';
include_once 'Models/Entities/Post.php';
include_once 'Models/Entities/Categoria.php';
include_once 'Models/Entities/Usuario.php';

class PostDAO {

	private $mysqli;

	function __construct() {
		$conexion = new Conexion();
		$this->mysqli = $conexion->conectar();
	}

	public function agregarPost(Post $post) {

		$id_usuario = $post->getId_usuario();
		$id_categoria = $post->getCategoria()->getId_categoria();
		$fecha_post = $post->getFecha_post();
		$titulo_post = $post->getTitulo_post();
		$imagen_post = $post->getImagen_post();
		$descripcion_post = $post->getDescripcion_post();
		$contenido_post = $post->getContenido_post();

		$sql = "insert into posts(id_usuario, id_categoria, fecha_post, titulo_post, imagen_post, descripcion_post, contenido_post) values($id_usuario, $id_categoria, '$fecha_post', '$titulo_post', '$imagen_post', '$descripcion_post', '$contenido_post')";

		$this->mysqli->query($sql);

		
		if ($this->mysqli->affected_rows > 0) {
			return true;
		}

		return false;
	}


	public function listarPost() {

		$sql = "select p.id_post, u.id_usuario, u.nombre_usuario, c.id_categoria, c.descripcion, p.fecha_post,
		        p.titulo_post, p.imagen_post, p.descripcion_post, p.contenido_post 
		        from posts p
		        join usuarios u on u.id_usuario=p.id_usuario
		        join categorias c on c.id_categoria=p.id_categoria";

		$respuesta = $this->mysqli->query($sql);

		$posts = [];
		while ($row = $respuesta->fetch_assoc()) {
			
			$post = new Post();
			$post->setId_post($row['id_post']);
			$usuario = new Usuario();
			$usuario->setNombre_usuario($row['id_usuario']);
			$post->setId_usuario($usuario);
			$categoria = new Categoria();
			$categoria->setDescripcion($row['descripcion']);
			$post->setCategoria($categoria);
			$post->setFecha_post($row['fecha_post']);
			$post->setTitulo_post($row['titulo_post']);
			$post->setImagen_post($row['imagen_post']);
			$post->setDescripcion_post($row['descripcion_post']);
			$post->setContenido_post($row['contenido_post']);

			$posts[] = $post;
		}

		$this->mysqli->close();

		return $posts;
	}

	public function eliminarPost($id) {

		$sql = "delete from posts where id_post = $id";

		$this->mysqli->query($sql);

		if ($this->mysqli->affected_rows > 0) {
			return true;
		}
		return false;
	}


	public function buscarPost($id) {

		$sql = "select p.id_post, u.id_usuario, u.nombre_usuario, c.id_categoria, c.descripcion, p.fecha_post,
		        p.titulo_post, p.imagen_post, p.descripcion_post, p.contenido_post 
		        from posts p
		        join usuarios u on u.id_usuario=p.id_usuario
		        join categorias c on c.id_categoria=p.id_categoria
		        where p.id_post = $id";

		$respuesta = $this->mysqli->query($sql);

		$row = $respuesta->fetch_assoc();

		if ($row == null) {
			return null;
		}

		$post = new Post();
			$post->setId_post($row['id_post']);
			$usuario = new Usuario();
			$usuario->setId_usuario($row['id_usuario']);
			$post->setId_usuario($usuario);
			$categoria = new Categoria();
			$categoria->setId_categoria($row['id_categoria']);
			$post->setCategoria($categoria);
			$post->setFecha_post($row['fecha_post']);
			$post->setTitulo_post($row['titulo_post']);
			$post->setImagen_post($row['imagen_post']);
			$post->setDescripcion_post($row['descripcion_post']);
			$post->setContenido_post($row['contenido_post']);

			
			return $post;
	}


	public function modificarPost(Post $post) {

		$id_usuario = $post->getId_usuario();
		$id_categoria = $post->getCategoria()->getId_categoria();
		$fecha_post = $post->getFecha_post();
		$titulo_post = $post->getTitulo_post();
		$imagen_post = $post->getImagen_post();
		$descripcion_post = $post->getDescripcion_post();
		$contenido_post = $post->getContenido_post();
		$id_post = $post->getId_post();

		$sql = "update posts set id_usuario = $id_usuario, id_categoria = $id_categoria, fecha_post = '$fecha_post', titulo_post = '$titulo_post', imagen_post = '$imagen_post', descripcion_post = '$descripcion_post', contenido_post = '$contenido_post' where id_post = $id_post";

		$this->mysqli->query($sql);

		
		if ($this->mysqli->affected_rows > 0) {
			return true;
		}
		return false;
	}
}
?>

